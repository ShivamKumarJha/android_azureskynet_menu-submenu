package shivam.menusubmenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        SubMenu veg=menu.addSubMenu("Vegetables");
        SubMenu fru=menu.addSubMenu("Friuits");

        veg.add(0,Menu.FIRST,Menu.NONE,"Tomato");
        veg.add(0,Menu.FIRST+1,Menu.NONE,"Onions");
        veg.add(0,Menu.FIRST+2,Menu.NONE,"Cabbage");
        fru.add(0,Menu.FIRST+3,Menu.NONE,"Apple");
        fru.add(0,Menu.FIRST+4,Menu.NONE,"Orange");
        fru.add(0,Menu.FIRST+5,Menu.NONE,"Mango");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
}
